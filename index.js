const express = require("express");
const app = express();
var http = require('http').createServer(app);
const bodyParser = require("body-parser");
const path = require("path");
const ip = require("ip");
const fs = require('fs');
const fetch = require('node-fetch');
//route
var route_partie = require('./server/routes/imothep');


const loadJSON = (filepath) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, 'utf8', (err, content) => {
      if (err) {
        reject(err)
      } else {
        try {
          resolve(JSON.parse(content));
        } catch (err) {
          reject(err)
        }
      }
    })
  });
}
//model
let partie = require('./server/model/partie')

//socket.io
var io = require('socket.io')(http);
let socketIdArray = []

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "/public")));
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);


app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname + "/imothep.html"));
});

app.use('/', route_partie);

io.on('connection', function (socket) {
  var address = socket.handshake.address
  address = address.split('::')
  socketIdArray.push(socket.id)
  socket.on('disconnect', function () {
    console.log('user disconnected' + address[1]);
  });

  console.log("a user connected at : " + address[1]);

  //recois la socket qui lance la partie de game()
  socket.on('debut de partie', function (deck) {
    var address = socket.handshake.address
    address = address
    console.log(address[2])
    let cleandb = {}
    fs.writeFile('./server/db/partie.json', cleandb, (err) => {
      if (err) throw err;
      console.log('Data written to file');
    });
    if (address[2] == '1' || address[1] == "ffff:127.0.0.1") {

      partie.pioche.d = deck.deck;
      partie.pioche.nbrCarte = deck.nbrCarte
      partie.pioche.defausse = deck.defausse


      console.log('deck ' + deck.deck.length);
      let data = JSON.stringify(partie, null, 2);

      fs.writeFile('./server/db/partie.json', data, (err) => {
        if (err) throw err;
        console.log('Data written to file');
      });

    } else {
      fs.readFile('', (err, data) => {
        if (err) throw err;
        partie = JSON.parse(data);
        console.log(partie);
      });
      /*   fetch(`http://${__dirname}/server/db/partie.json`)
          .then(data => partie.pioche = data)
          .then(socket.emit('init 2nd client', partie))
          .then(console.log(partie)) */
      /*  socket.to(socketIdArray[1]).emit('init 2nd client', partie.pioche) */

    }

  });

  //recois la demande de pioche du client
  socket.on('pioche', function (deck) {
    let data = JSON.stringify(deck, null, 2);

    fs.writeFile('/server/db/partie.json', data, (err) => {
      if (err) throw err;
      console.log('Data written to file');
    });
  })

  //verif sir la versions de l'objet partie du client est la meme que celle du server
  socket.on('verif deck', function (deck) {
    fs.readFile('', (err, data) => {
      if (err) throw err;
      partie = JSON.parse(data);
      loadJSON(`http://${__dirname}/server/db/partie.json`) //essaie d'ecriture readJson en asynchrone with a promise
        .then(console.log)
        .catch(console.log);
      console.log(partie);
    });
  })

  socket.on('fin de tour', function (partie) { //enregistre l'etat de la partie dans la db
    let data = JSON.stringify(partie, null, 2);

    fs.writeFile('./server/db/partie.json', data, (err) => {
      if (err) throw err;
      console.log('Data written to file');
    });
  })

  //code pour la partier multi
  /*  socket.on('debut de tour', function () {
     if (address[2] == 1 || address[3] == "127.0.0.1") {
       // socket.to().emit() pour le deuxieme client

     } else {
       ///// socket.to().emit() c'est le tour 

     }
   }) */
});



http.listen(3000, function () {
  console.log("Example app listening on port 3000!" + "mon addresse :" + ip.address());

});