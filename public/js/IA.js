let dieuMarron = -1;
let dieuVert = -1;
let dieuBleu = -1;
let dieuRose = -1;

let ressourceMarron = -1;
let ressourceVert = -1;
let ressourceBleu = -1;
let ressourceRose = -1;

let mainCarriere = -1;
let mainChaland = -1;
let mainTailleur = -1;

let tarrainCarriere = 0;
let terrainChaland = 0;
let terrainTailleur = 0;

let startMarron = -1;
let startVert = -1;
let startBleu = -1;
let startRose = -1;

let piegeMarron = -1;
let piegeVert = -1;
let piegeBleu = -1;
let piegeRose = -1;

// Identifie quelles cartes sont dans la main de l'ia
for (let i = 0; i < ia.main.lenght; i++) {
    // CODE POUR VERIFIER LES DIEUX
    if (ia.main[i].type == "dieu" && ia.main[i].couleur == "marron") {
        dieuMarron = i;
    }
    if (ia.main[i].type == "dieu" && ia.main[i].couleur == "vert") {
        dieuVert = i;
    }
    if (ia.main[i].type == "dieu" && ia.main[i].couleur == "bleu") {
        dieuMarron = i;
    }
    if (ia.main[i].type == "dieu" && ia.main[i].couleur == "rose") {
        dieuVert = i;
    }

    // CODE POUR VERIFIER LES RESSOURCES
    if (ia.main[i].type == "ressource" && ia.main[i].couleur == "marron") {
        ressourceMarron = i;
    }
    if (ia.main[i].type == "ressource" && ia.main[i].couleur == "vert") {
        ressourceVert = i;
    }
    if (ia.main[i].type == "ressource" && ia.main[i].couleur == "bleu") {
        ressourceBleu = i;
    }
    if (ia.main[i].type == "ressource" && ia.main[i].couleur == "rose") {
        ressourceRose = i;
        if (ia.main[i].nom == "carriere") {
            mainCarriere = i;
        }
        if (ia.main[i].nom == "chaland") {
            mainChaland = i;
        }
        if (ia.main[i].nom == "tailleur") {
            mainTailleur = i;
        }
    }

    // CODE POUR VERIFIER LES STARTS
    if (ia.main[i].type == "start" && ia.main[i].couleur == "marron") {
        startMarron = i;
    }
    if (ia.main[i].type == "start" && ia.main[i].couleur == "vert") {
        startVert = i;
    }
    if (ia.main[i].type == "start" && ia.main[i].couleur == "bleu") {
        startBleu = i;
    }
    if (ia.main[i].type == "start" && ia.main[i].couleur == "rose") {
        startRose = i;
    }

    // CODE POUR VERIFIER LES PIEGES
    if (ia.main[i].type == "piege" && ia.main[i].couleur == "marron") {
        startMarron = i;
    }
    if (ia.main[i].type == "piege" && ia.main[i].couleur == "vert") {
        startVert = i;
    }
    if (ia.main[i].type == "piege" && ia.main[i].couleur == "bleu") {
        startBleu = i;
    }
    if (ia.main[i].type == "piege" && ia.main[i].couleur == "rose") {
        startRose = i;
    }
}

if (partie.terrainJ2.rose[1].length > 0) {
    for (let i = 0; i < partie.terrainJ2.rose[1].length; i++) {
        if (ia.main[i].nom == "carriere") {
            terrainCarriere = 1;
        }
        if (ia.main[i].nom == "chaland") {
            terrainChaland = 1;
        }
        if (ia.main[i].nom == "tailleur") {
            terrainTailleur = 1;
        }
    }
}

let dessusStartMarron = partie.terrainJ2.marron[0].lenght - 1;
let dessusStartVert = partie.terrainJ2.vert[0].lenght - 1;
let dessusStartBleu = partie.terrainJ2.bleu[0].lenght - 1;
let dessusStartRose = partie.terrainJ2.rose[0].lenght - 1;


if (dieuMarron != -1) {
    /* SI DIEU MARRON en main */
    //ALORS DIEU MARRON sur le terrain

} else if (dieuVert != -1 && ressourceMarron != -1 && (startMarron != -1 || dieuMarron != -1)) {
    //SINON SI(DIEU VERT en main ET au moins une RESSOURCE MARRON ET(START MARRON OU DIEU MARRON))
    //ALORS DIEU VERT sur le terrain

} else if (dieuBleu != -1 && ressourceVert != -1 && (startMarron != -1 || dieuMarron != -1) && (startVert != -1 || dieuVert != -1)) {
    //SINON SI(DIEU BLEU en main ET au moins une RESSOURCE VERT ET(START MARRON OU DIEU MARRON) ET(START VERT OU DIEU VERT)
    //ALORS DIEU BLEU sur le terrain

} else if (dieuRose != -1 && ((partie.terrainJ2.marron[1].length + partie.terrainJ2.vert[1].length + partie.terrainJ2.bleu[1].length) >= 10 && (startMarron != -1 || dieuMarron != -1) && (startVert != -1 || dieuVert != -1) && (startBleu != -1 || dieuBleu != -1))) {
    //SINON SI(DIEU ROSE en main ET(TAILLE RESSOURCE VERT + TAILLE RESSOURCE BLEU + TAILLE RESSOURCE ROSE) SUPERIEUR EGAL 10 ET(START MARRON OU DIEU MARRON) ET(START VERT OU DIEU VERT) ET(START BLEU OU DIEU BLEU))
    //ALORS DIEU ROSE sur le terrain

} else if (piegeMarron != -1 && partie.terrainJ1.marron[0].lenght > 0) {
    //SINON SI PIEGE MARRON en main et J1 a START MARRON
    //ALORS PIEGE MARRON dans la gueule du J1

} else if (piegeVert != -1 && partie.terrainJ1.vert[0].lenght > 0) {
    //SINON SI PIEGE VERT en main et J1 a START VERT
    //ALORS PIEGE VERT dans la gueule du J1

} else if (piegeBleu != -1 && partie.terrainJ1.bleu[0].lenght > 0) {
    //SINON SI PIEGE BLEU en main et J1 a START BLEU
    //ALORS PIEGE BLEU dans la gueule du J1

} else if (piegeRose != -1 && partie.terrainJ1.rose[0].lenght > 0) {
    //SINON SI PIEGE ROSE en main et J1 a START ROSE
    //ALORS PIEGE ROSE dans la gueule du J1

} else if (startMarron != -1 && partie.terrainJ2.marron[0][dessusStartMarron].type == "start" && partie.terrainJ2.marron[0][dessusStartMarron].type == "dieu") {
    //SINON SI START MARRON en main ET pas de START MARRON ET pas de DIEU MARRON
    //ALORS START MARRON sur le terrain

} else if (startVert != -1 && partie.terrainJ2.vert[0][dessusStartVert].type == "start" && partie.terrainJ2.vert[0][dessusStartVert].type == "dieu") {
    //SINON SI START VERT en main ET pas de START VERT ET pas de DIEU VERT
    //ALORS START VERT sur le terrain

} else if (startBleu != -1 && partie.terrainJ2.bleu[0][dessusStartBleu].type == "start" && partie.terrainJ2.bleu[0][dessusStartBleu].type == "dieu") {
    //SINON SI START BLEU en main ET pas de START BLEU ET pas de DIEU BLEU 
    //ALORS START BLEU sur le terrain

} else if (startRose != -1 && partie.terrainJ2.rose[0][dessusStartRose].type == "start" && partie.terrainJ2.rose[0][dessusStartRose].type == "dieu") {
    //SINON SI START ROSE en main ET pas de START ROSE ET pas de DIEU ROSE
    //ALORS START ROSE sur le terrain

} else if (mainCarriere != -1 && (partie.terrainJ2.rose[0][dessusStartRose].type == "start" || partie.terrainJ2.rose[0][dessusStartRose].type == "dieu") && terrainCarriere != 1) {
    //SINON SI RESSOURCE ROSE en main ET(START ROSE sur le terrain OU DIEU ROSE sur terrain) ET pas la même RESSOURCE ROSE sur le terrain
    //ALORS RESSOURCE ROSE sur le terrain

} else if (mainChaland != -1 && (partie.terrainJ2.rose[0][dessusStartRose].type == "start" || partie.terrainJ2.rose[0][dessusStartRose].type == "dieu") && terrainChaland != 1) {
    //SINON SI RESSOURCE ROSE en main ET(START ROSE sur le terrain OU DIEU ROSE sur terrain) ET pas la même RESSOURCE ROSE sur le terrain
    //ALORS RESSOURCE ROSE sur le terrain

} else if (mainTailleur != -1 && (partie.terrainJ2.rose[0][dessusStartRose].type == "start" || partie.terrainJ2.rose[0][dessusStartRose].type == "dieu") && terrainTailleur != 1) {
    //SINON SI RESSOURCE ROSE en main ET(START ROSE sur le terrain OU DIEU ROSE sur terrain) ET pas la même RESSOURCE ROSE sur le terrain
    //ALORS RESSOURCE ROSE sur le terrain

} else if (ressourceBleu != -1 && (partie.terrainJ2.bleu[0][dessusStartBleu].type == "start" || partie.terrainJ2.bleu[0][dessusStartBleu].type == "dieu")) {
    //SINON SI RESSOURCE BLEU en main ET(START BLEU sur le terrain OU DIEU BLEU sur terrain)
    //ALORS RESSOURCE BLEU sur le terrain
} else if (ressourceVert != -1 && (partie.terrainJ2.vert[0][dessusStartVert].type == "start" || partie.terrainJ2.vert[0][dessusStartVert].type == "dieu")) {
    //SINON SI RESSOURCE VERT en main ET(START VERT sur le terrain OU DIEU VERT sur terrain)
    //ALORS RESSOURCE VERT sur le terrain
} else if (ressourceMarron != -1 && (partie.terrainJ2.marron[0][dessusStartMarron].type == "start" || partie.terrainJ2.marron[0][dessusStartMarron].type == "dieu")) {
    //SINON SI RESSOURCE MARRON en main ET(START MARRON sur le terrain OU DIEU MARRON sur terrain)
    //ALORS RESSOURCE MARRON sur le terrain
} else if (mainCarriere != -1 && (partie.terrainJ2.bleu[1].length === 0 || terrainCarriere === 1)) {
    //SINON SI RESSOURCE ROSE dans la main ET (pas de RESSOURCE BLEU sur le terrain OU RESSOURCE ROSE identique sur le terrain)
    //ALORS RESSOURCE ROSE défaussée
} else if (mainChaland != -1 && (partie.terrainJ2.bleu[1].length === 0 || terrainChaland === 1)) {
    //SINON SI RESSOURCE ROSE dans la main ET (pas de RESSOURCE BLEU sur le terrain OU RESSOURCE ROSE identique sur le terrain)
    //ALORS RESSOURCE ROSE défaussée
} else if (mainTailleur != -1 && (partie.terrainJ2.bleu[1].length === 0 || terrainTailleur === 1)) {
    //SINON SI RESSOURCE ROSE dans la main ET (pas de RESSOURCE BLEU sur le terrain OU RESSOURCE ROSE identique sur le terrain)
    //ALORS RESSOURCE ROSE défaussée
} else if (ressourceBleu != 1 && (partie.terrainJ2.bleu[1].length > 0 || ((partie.terrainJ2.vert[0][dessusStartVert].type != "start" && partie.terrainJ2.vert[0][dessusStartVert].type != "piege") || partie.terrainJ2.vert[0][dessusStartVert].type == "piege"))) {
    //SINON SI RESSOURCE BLEU dans la main ET (au moins un RESSOURCE BLEU sur le terrain OU (rien sur la case START VERT OU PIEGE VERT sur le terrain))
    //ALORS RESSOURCE BLEU défaussée
} else if (ressourceVert != 1 && (partie.terrainJ2.vert[1].length > 0 || partie.terrainJ2.marron[0].length === 0)) {
    //SINON SI RESSOURCE VERTE dans la main ET (au moins un RESSOURCE VERT sur le terrain OU rien sur la case START MARRON)
    //ALORS RESSOURCE VERTE défaussée
} else if (ressourceMarron != 1 && partie.terrainJ2.marron[1].length > 0) {
    //SINON SI RESSOURCE MARRON dans la main ET au moins une RESSOURCE MARRON sur le terrain
    //ALORS RESSOURCE MARRON défaussée
} else if (startRose != 1 && (partie.terrainJ2.rose[0].type == "start" || partie.terrainJ2.rose[0].type == "dieu")) {
    //SINON SI START ROSE en main ET (DIEU ROSE sur le terrain OU START ROSE sur le terrain)
    //ALORS START ROSE défaussée
} else if (startBleu != 1 && (partie.terrainJ2.bleu[0].type == "start" || partie.terrainJ2.bleu[0].type == "dieu")) {
    //SINON SI START BLEU en main ET (DIEU BLEU sur le terran OU START BLEU sur le terrain)
    //ALORS START BLEU défaussée
} else if (startVert != 1 && (partie.terrainJ2.vert[0].type == "start" || partie.terrainJ2.vert[0].type == "dieu")) {
    //SINON SI START VERT en main ET (DIEU VERT sur le terran OU START VERT sur le terrain)
    //ALORS START VERT défaussée
} else if (startMarron != 1 && (partie.terrainJ2.marron[0].type == "start" || partie.terrainJ2.marron[0].type == "dieu")) {
    //SINON SI START MARRON en main ET (DIEU MARRON sur le terran OU START MARRON sur le terrain)
    //ALORS START MARRON défaussée
} else if (piegeRose != 1) {
    //SINON SI PIEGE ROSE en main
    //ALORS PIEGE ROSE défaussée
} else if (piegeBleu != 1) {
    //SINON SI PIEGE BLEU en main
    //ALORS PIEGE BLEU défaussée
} else if (piegeVert != 1) {
    //SINON SI PIEGE VERT en main
    //ALORS PIEGE VERT défaussée
} else if (piegeVert != 1) {
    //SINON SI PIEGE MARRON en main
    //ALORS PIEGE MARRON défaussée
}