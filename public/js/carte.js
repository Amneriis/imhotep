class Carte {
    constructor(nom, type_carte = new type(), couleur, url) {
        this.nom = nom;
        this.type = type_carte;
        this.couleur = couleur;
        this.urlImg = url;
        this.img
    }

    start() {
        if (this.type == "start" && this.couleur == "marron") {
            //alors débloque la pose des maisons
        }
    }


}
// Set de jeu Egyptiens
//Cartes marrons
const puits = new Carte("puits", "start", "marron", "./img/cartes/Cartes_egyptiennes/PUITS.png")
const cholera = new Carte("cholera", "piege", "marron", "./img/cartes/Cartes_egyptiennes/CHOLERA.png")
const maison = new Carte("maison", "ressource", "marron", "./img/cartes/Cartes_egyptiennes/MAISON.png")
const medecin = new Carte("medecin", "dieu", "marron", "./img/cartes/Cartes_egyptiennes/MEDECIN.png")

//Cartes vertes
const crue = new Carte("crue", "start", "vert", "./img/cartes/Cartes_egyptiennes/CRUE.png")
const secheresse = new Carte("secheresse", "piege", "vert", "./img/cartes/Cartes_egyptiennes/SECHERESSE.png")
const figue = new Carte("figue", "ressource", "vert", "./img/cartes/Cartes_egyptiennes/FIGUE.png")
const ail = new Carte("ail", "ressource", "vert", "./img/cartes/Cartes_egyptiennes/AIL.png")
const ble = new Carte("ble", "ressource", "vert", "./img/cartes/Cartes_egyptiennes/BLE.png")
const orge = new Carte("orge", "ressource", "vert", "./img/cartes/Cartes_egyptiennes/ORGE.png")
const roseau = new Carte("roseau", "ressource", "vert", "./img/cartes/Cartes_egyptiennes/ROSEAU.png")
const lin = new Carte("lin", "ressource", "vert", "./img/cartes/Cartes_egyptiennes/LIN.png")
const osiris = new Carte("osiris", "dieu", "vert", "./img/cartes/Cartes_egyptiennes/OSIRIS.png")

//Cartes bleues
const atelier = new Carte("atelier", "start", "bleu", "./img/cartes/Cartes_egyptiennes/ATELIER.png")
/*const incendie = new Carte("incendie", "piege", "bleu", "./img/cartes/Cartes_egyptiennes/INCENDIE.png")*/
const tissu = new Carte("tissu", "ressource", "bleu", "./img/cartes/Cartes_egyptiennes/TISSU.png")
const bouza = new Carte("bouza", "ressource", "bleu", "./img/cartes/Cartes_egyptiennes/BOUZA.png")
const papyrus = new Carte("papyrus", "ressource", "bleu", "./img/cartes/Cartes_egyptiennes/PAPYRUS.png")
const pompier = new Carte("pompier", "dieu", "bleu", "./img/cartes/Cartes_egyptiennes/POMPIER.png")

//Cartes rouges
const pyramide = new Carte("pyramide", "start", "rouge", "./img/cartes/Cartes_egyptiennes/PYRAMIDE.jpg")
const seisme = new Carte("seisme", "piege", "rouge", "./img/cartes/Cartes_egyptiennes/GLISSEMENT.png")
const carriere = new Carte("carriere", "ressource", "rouge", "./img/cartes/Cartes_egyptiennes/CARRIERE.png")
const tailleur = new Carte("tailleur", "ressource", "rouge", "./img/cartes/Cartes_egyptiennes/TAILLEUR.png")
const chaland = new Carte("chaland", "ressource", "rouge", "./img/cartes/Cartes_egyptiennes/CHALAND.png")
const architecte = new Carte("architecte", "dieu", "rouge", "./img/cartes/Cartes_egyptiennes/ARCHITECTE.png")

//Set de jeu Nordique
//Cartes marrons
const hache = new Carte("hache", "start", "marron", "./img/cartes/Cartes_medievale/Hache.png")
const bois = new Carte("bois", "ressource", "marron", "./img/cartes/Cartes_medievale/Bois.png")
const feu = new Carte("feu", "piege", "marron", "./img/cartes/Cartes_medievale/Feu.png")
const pluie = new Carte("pluie", "dieu", "marron", "./img/cartes/Cartes_medievale/Pluie.png")

//Cartes vertes
const sac = new Carte("sac", "start", "vert", "./img/cartes/Cartes_medievale/Sac.png")
const citron = new Carte("citron", "ressource", "vert", "./img/cartes/Cartes_medievale/Citron.png")
const banane = new Carte("banane", "ressource", "vert", "./img/cartes/Cartes_medievale/Banane.png")
const pomme = new Carte("pomme", "ressource", "vert", "./img/cartes/Cartes_medievale/Pomme.png")
const poire = new Carte("poire", "ressource", "vert", "./img/cartes/Cartes_medievale/Poire.png")
const coffreFort = new Carte("sac", "dieu", "vert", "./img/cartes/Cartes_medievale/Sac.png")
const voleur = new Carte("voleur", "piege", "vert", "./img/cartes/Cartes_medievale/Voleur.png")

//Cartes bleues
const pioche = new Carte("pioche", "start", "bleu", "./img/cartes/Cartes_medievale/Pioche.png")
const pierre = new Carte("pierre", "ressource", "bleu", "./img/cartes/Cartes_medievale/Pierre.png")
const cristal = new Carte("cristal", "ressource", "bleu", "./img/cartes/Cartes_medievale/Cristal.png")
const or = new Carte("or", "ressource", "bleu", "./img/cartes/Cartes_medievale/Or.png")
const diamant = new Carte("diamant", "ressource", "bleu", "./img/cartes/Cartes_medievale/Diamant.png")
const BlessureBleue = new Carte("blessure", "piege", "bleu", "./img/cartes/Cartes_medievale/BlessureBleue.png")
const minageInfini = new Carte("diamant", "dieu", "bleu", "./img/cartes/Cartes_medievale/MinageInfini.png")

//Cartes rouges
const marteau = new Carte("marteau", "start", "rouge", "./img/cartes/Cartes_medievale/Marteau.png")
const epee = new Carte("epee", "ressource", "rouge", "./img/cartes/Cartes_medievale/Epee.png")
const armure = new Carte("armure", "ressource", "rouge", "./img/cartes/Cartes_medievale/Armure.png")
const casque = new Carte("casque", "ressource", "rouge", "./img/cartes/Cartes_medievale/Casque.png")
const blessure = new Carte("blessure", "piege", "rouge", "./img/cartes/Cartes_medievale/Blessure.png")
const forceInfinie = new Carte("diamant", "dieu", "rouge", "./img/cartes/Cartes_medievale/ForceInfinie.png")