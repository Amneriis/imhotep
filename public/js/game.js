let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
let socket = io();
// Position X du canvas
let elemLeft = canvas.offsetLeft;
// Position Y du canvas
let elemTop = canvas.offsetTop

let x1 = 0; //valeur
let y1 = 0;

//
let deck

//
let compteurPioche = 0


//object qui enregistre la partie coté client et qui sera comparé a la version server
const partie = {
    "pioche": {
        "d": [],
        "nbrCarte": "",
        "defausse": []
    },
    "joueur1": {},
    "joueur1": {},
    terrainJ1,
    terrainJ2,
    "nbrTour": 0
}

//compteur qui permet  gerer les mouseclick selon les menu
let lvlmenu = 0;

//Tableau de promise d'image preload
const ressourcesPromises = [];

//Var joueur
const P1 = new Joueur("bertyn")
const P2 = new Joueur("bob")

//Fonction pour précharger les objets
function preload(url, promisesList) {
    let img = new Image()
    let promise = new Promise((resolve, reject) => {
        img.onload = function () {
            resolve()
            console.log(url + "image chargé");
        };
        img.src = url
    })
    promisesList.push(promise)
    return img
}


//IMG BG preload
/* let play = preload("img/play.png", ressourcesPromises) */
let bgAccueil = preload("img/background_menu.png", ressourcesPromises)
let bgGame = preload("img/board.png", ressourcesPromises)
let rule1 = preload("img/rules.png", ressourcesPromises)
let rule2 = preload("img/rules2.png", ressourcesPromises)

//IMG Screen victoire / défaite
let winScreen = preload("img/win.png", ressourcesPromises)
let looseScreen = preload("img/loose.png", ressourcesPromises)

//dos de carte 
let dosDeCarte = preload("img/cartes/Cartes_egyptiennes/DOS_DE_CARTE.png", ressourcesPromises)


//Carte Preload Marron Egyptien
puits.img = preload(puits.urlImg, ressourcesPromises)
cholera.img = preload(cholera.urlImg, ressourcesPromises)
maison.img = preload(maison.urlImg, ressourcesPromises)
medecin.img = preload(medecin.urlImg, ressourcesPromises)


//Carte Preload Verte Egyptien
crue.img = preload(crue.urlImg, ressourcesPromises)
secheresse.img = preload(secheresse.urlImg, ressourcesPromises)
figue.img = preload(figue.urlImg, ressourcesPromises)
ail.img = preload(ail.urlImg, ressourcesPromises)
ble.img = preload(ble.urlImg, ressourcesPromises)
orge.img = preload(orge.urlImg, ressourcesPromises)
roseau.img = preload(roseau.urlImg, ressourcesPromises)
lin.img = preload(lin.urlImg, ressourcesPromises)
osiris.img = preload(osiris.urlImg, ressourcesPromises)


//Carte Preload Bleue Egyptien
preload(atelier.urlImg, ressourcesPromises)
/* preload(incendie.urlImg, ressourcesPromises) */
tissu.img = preload(tissu.urlImg, ressourcesPromises)
bouza.img = preload(bouza.urlImg, ressourcesPromises)
papyrus.img = preload(papyrus.urlImg, ressourcesPromises)
pompier.img = preload(pompier.urlImg, ressourcesPromises)


//Carte Preload Rouges Egyptien

/* preload(pyramide.urlImg, ressourcesPromises) */
seisme.img = preload(seisme.urlImg, ressourcesPromises)
carriere.img = preload(carriere.urlImg, ressourcesPromises)
tailleur.img = preload(tailleur.urlImg, ressourcesPromises)
chaland.img = preload(chaland.urlImg, ressourcesPromises)

//Carte Preload Marron Medievale
hache.img = preload(hache.urlImg, ressourcesPromises)
bois.img = preload(bois.urlImg, ressourcesPromises)

feu.img = preload(feu.urlImg, ressourcesPromises)
pluie.img = preload(pluie.urlImg, ressourcesPromises)

//Carte Preload Verte Medievale
/* sac.img = preload(sac.urlImg, ressourcesPromises)
citron.img = preload(citron.urlImg, ressourcesPromises)
banane.img = preload(banane.urlImg, ressourcesPromises)
pomme.img = preload(pomme.urlImg, ressourcesPromises)
poire.img = preload(poire.urlImg, ressourcesPromises)

coffreFort.img = preload(coffreFort.urlImg, ressourcesPromises)
voleur.img = preload(voleur.urlImg, ressourcesPromises)

//Carte Preload Bleue Medievale
pioche.img = preload(pioche.urlImg, ressourcesPromises)
pierre.img = preload(pierre.urlImg, ressourcesPromises)
cristal.img = preload(cristal.urlImg, ressourcesPromises)
or.img = preload(or.urlImg, ressourcesPromises)
diamant.img = preload(diamant.urlImg, ressourcesPromises)
/* preload(blessureBleue.urlImg, ressourcesPromises) */
/* minageInfini.img = preload(minageInfini.urlImg, ressourcesPromises)

//Carte Preload Rouge Medievale
marteau.img = preload(marteau.urlImg, ressourcesPromises)
epee.img = preload(epee.urlImg, ressourcesPromises)
armure.img = preload(armure.urlImg, ressourcesPromises)
casque.img = preload(casque.urlImg, ressourcesPromises)
blessure.img = preload(blessure.urlImg, ressourcesPromises)
forceInfinie.img = preload(forceInfinie.urlImg, ressourcesPromises)  */

function pageAccueil() {
    //commande qui affiche l'image de background
    ctx.drawImage(bgAccueil, 0, 0, 1000, 800)

    mouseclick(lvlmenu)
    //Jouer (boutton)
    //Règles (boutton)
    //Options (boutton)
}
Promise.all(ressourcesPromises).then(pageAccueil)

function pageRegles(nbr) {
    if (nbr == 1) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        //Règles (texte)
        ctx.drawImage(rule1, 0, 0, 1000, 850)
        //Retour (boutton)
    } else if (nbr == 2) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        //Règles (texte)
        ctx.drawImage(rule2, 0, 0, 1000, 850)
        //Retour (boutton)
    }



}


function mouseclick(lvlmenu) {
    canvas.addEventListener('click', function (event) {
        console.log(`coté x ${event.pageX}`)
        console.log(`coté y ${event.pageY}`)
        // Position X du click (Position X du click sur la page moins la position X du canvas)
        x = event.pageX,
            // Position Y du click (Position Y du click sur la page moins la position Y du canvas)
            y = event.pageY;

        //console.log('click position on canvas', x, y); // Optionnel
        //Mooseclick accueil
        if (lvlmenu == 0) {
            if ((x / 1000 < 0.915 && x / 1000 > 0.4075) && (y / 1000 < 0.32 && y / 1000 > 0.182)) {
                //reinitialise le canvas
                pageRegles(1)
                lvlmenu = 1; //change la valeur de lvlmenu afin de changer les mouseclicks
                console.log("start")

            } else if ((x / 1000 < 0.915 && x / 1000 > 0.4075) && (y / 1000 < 0.51 && y / 1000 > 0.374)) {
                lvlmenu = 1;
                console.log("option");
            } else if ((x / 1000 < 0.915 && x / 1000 > 0.40375) && (y / 1000 < 0.701 && y / 1000 > 0.562)) {
                lvlmenu = 1;
                console.log("vous avez quittez le jeu")
            }
        } else if (lvlmenu == 1) { //Mouseclick ecran d'explication

            if ((x < 947 && x > 715) && (y < 797 && y > 725)) {
                pageRegles(2)
                lvlmenu = 2;

            }
            /*    if (((x < 154 && x > 64) && (y < 459 && y > 320)) && partie.nbrPioche < 1) {
                     JouerCarte(partie.pioche.defausse.pop(), "recupDefausse") 
                   partie.nbrPioche++
               } */

        } else if (lvlmenu == 2) { //Mouseclick ecran d'explication 

            if ((x < 933 && x > 704) && (y < 797 && y > 725)) {
                game()
                lvlmenu = 3
            }
        } else if (lvlmenu == 3) {
            if (((x < 1055 && x > 889) && (y < 480 && y > 320)) && compteurPioche < 1) {

                if (joueurtour(partie.nbrTour) == 'j1') {
                    affichPioche(P1)
                } else if (joueurtour(partie.nbrTour) == 'j2') {
                    affichPioche(P2)
                }
            }

        }



    });

}

function game() {
    ctx.clearRect(0, 0, canvas.width, canvas.height); //vide le canvas
    ctx.drawImage(bgGame, 0, 0, 1000, 800) //affiche le background
    ctx.drawImage(dosDeCarte, 850, 342, 100, 115) //dos de carte du deck

    deck = new Deck([puits, cholera, maison, medecin,
        crue, secheresse, figue, ail, ble, orge, roseau, lin, osiris,
        atelier, incendie,
        tissu, bouza, papyrus, pompier,
        pyramide, seisme, carriere, tailleur, chaland, architecte
    ])
    partie.pioche = deck; // le rajoute a l'object partie
    partie.pioche.init() //initialise le deck 
    partie.pioche.melange() // le melange 

    P1.MainDepart(partie.pioche) //initialise la main du joueur 1 en piochant 5 carte
    P2.MainDepart(partie.pioche) //initialise la main du joueur 2 en piochant 5 carte

    socket.emit('debut de partie', deck); //envoye une requete au serveur afin de remplir le json 
    tour()





    /*--------------------------------------------------------------------------------------------------------------*/
    //socket test multi
    /* socket.on('init 2nd client', function (data) {
        deck.deck = data.deck
        partie.pioche = data.pioche;
        console.log("client joueur 2")
    })*/
}

function affichMain(Player) {
    for (let i = 0; i < Player.main.length; i++) {
        let carteElt = document.createElement("div");
        carteElt.innerHTML = '<button>Jouer</button>';
        carteElt.innerHTML += '<p><img class="carte" src="' + Player.main[i].urlImg + '" alt="' + Player.main[i].nom + '"></p>';
        carteElt.innerHTML += '<button>Défausser</button>';
        carteElt.childNodes[0].addEventListener("click", function (e) {
            if (compteurPioche == 1) {
                if (regle(Player.main[i], joueurtour(partie.nbrTour))) {
                    e.target.parentNode.parentNode.removeChild(e.target.parentNode);
                    partie.nbrTour++
                }
            }
        })
        carteElt.childNodes[2].addEventListener("click", function (e) {
            if (compteurPioche == 1) {
                partie.pioche.defausse.push(Player.main[i]);
                JouerCarte(Player.main[i], "defausser")
                retireCarteMain(Player.main, i)
                e.target.parentNode.parentNode.removeChild(e.target.parentNode);


            }
        })
        document.getElementById("main").appendChild(carteElt);


    }
    console.log("main " + Player.nom)
}

function affichPioche(player) {
    compteurPioche++
    player.main.push(partie.pioche.piocher())
    console.log(player.main)
    document.getElementById("main").innerHTML = "" //vide le div main
    affichMain(player)

}

function retireCarteMain(main, index) { //functrion qui retire la carte defausse de la main du joueur
    main.splice(index, 1)
}

function joueurtour(nbrTour) {
    if (nbrTour % 2 == 0) {
        return "j1"
    } else {
        return "j2"
    }
}

function tour() {
    if (win() != true) {
        if (joueurtour(partie.nbrTour) == "j1") {
            compteurPioche = 0
            mouseclick(1)
            //socket emit qui dit au  localhost que c'est sont tour de joué
            /*  socket.emit('debut de tour', partie) */
            document.getElementById("main").innerHTML = "" // vide la div main
            affichMain(P1)

            socket.emit('fin de tour', partie)
            if (compteurPioche == 2) {
                partie.nbrTour++
                tour()
            }

        } else if (joueurtour(partie.nbrTour) == "j2") {
            compteurPioche = 0
            mouseclick(1)
            /* socket.emit('debut de tour', partie) */

            document.getElementById("main").innerHTML = "" //vide le div main
            affichMain(P2)
            socket.emit('fin de tour', partie)
            if (compteurPioche == 2) {
                partie.nbrTour++
                tour()
            }

        }

    }


}

function win() {
    if (partie.terrainJ1.pink[1].length == 3 || partie.terrainJ2.pink[1].length == 3) {
        return true
    }

}

function JouerCarte(carteaJouer, play) { //function qui gere l'affichage des carte sur la terrain et la defausse
    if (play == "defausser") {
        ctx.drawImage(carteaJouer.img, 54, 342, 100, 115)

    } else if (play == "jouer") {
        if (joueurtour(partie.nbrTour) == "j1") {
            if (carteaJouer.couleur == "marron") {
                if (carteaJouer.type == "piege" || carteaJouer.type == "start" || carteaJouer.type == "dieu") {
                    x1 = 245;
                    y1 = 432;
                } else if (carteaJouer.type == "ressource") {
                    x1 = 245;
                    y1 = 566;
                }

            } else if (carteaJouer.couleur == "vert") {
                if (carteaJouer.type == "piege" || carteaJouer.type == "start" || carteaJouer.type == "dieu") {

                    x1 = 388;
                    y1 = 432;
                } else if (carteaJouer.type == "ressource") {
                    x1 = 388;
                    y1 = 566;
                }


            } else if (carteaJouer.couleur == "bleu") {
                if (carteaJouer.type == "piege" || carteaJouer.type == "start" || carteaJouer.type == "dieu") {

                    x1 = 532;
                    y1 = 432;
                } else if (carteaJouer.type == "ressource") {
                    x1 = 532;
                    y1 = 566;
                }

            } else if (carteaJouer.couleur == "rose") {
                if (carteaJouer.type == "piege" || carteaJouer.type == "start" || carteaJouer.type == "dieu") {

                    x1 = 686;
                    y1 = 432;
                } else if (carteaJouer.type == "ressource") {
                    x1 = 686;
                    y1 = 566;
                }

            }
        }
        if (joueurtour(partie.nbrTour) == "j2") {
            if (carteaJouer.couleur == "marron") {
                if (carteaJouer.type == "piege" || carteaJouer.type == "start" || carteaJouer.type == "dieu") {
                    x1 = 245;
                    y1 = 265;
                } else if (carteaJouer.type == "ressource") {
                    x1 = 245;
                    y1 = 135;
                }

            } else if (carteaJouer.couleur == "vert") {
                if (carteaJouer.type == "piege" || carteaJouer.type == "start" || carteaJouer.type == "dieu") {
                    x1 = 388;
                    y1 = 265;
                } else if (carteaJouer.type == "ressource") {
                    x1 = 388;
                    y1 = 135;
                }

            } else if (carteaJouer.couleur == "bleu") {
                if (carteaJouer.type == "piege" || carteaJouer.type == "start" || carteaJouer.type == "dieu") {
                    x1 = 532;
                    y1 = 265;
                } else if (carteaJouer.type == "ressource") {
                    x1 = 532;
                    y1 = 135;
                }


            } else if (carteaJouer.couleur == "rose") {
                if (carteaJouer.type == "piege" || carteaJouer.type == "start" || carteaJouer.type == "dieu") {
                    x1 = 686;
                    y1 = 265;
                } else if (carteaJouer.type == "ressource") {
                    x1 = 686;
                    y1 = 135;
                }
            }

            ctx.drawImage(carteaJouer.img, x, y, 100, 115) //dessine la carte au bon endroit
        }
    }


}



function regle(carte, joueur) {
    // renvoie un boulean si la carte peut etre joué ou pas
    if (joueur == "j1") {
        if (carte.type == "start") {
            console.log("start")
            if (carte.couleur == "marron") {
                //vert est debloqué
                // jouer la carte
                partie.terrainJ1.brown[0].push(carte)
                JouerCarte(carte, "jouer")


            } else if (carte.couleur == "vert" && debloquerLaCouleurSuivante(partie.terrainJ1.brown[0], 1)) {
                partie.terrainJ1.green[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "bleu" && debloquerLaCouleurSuivante(partie.terrainJ1.green[0], 1)) {
                partie.terrainJ1.blue[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "rouge" && debloquerLaCouleurSuivante(partie.terrainJ1.pink[0], 10)) {
                partie.terrainJ1.pink[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            }
        } else if (carte.type == "ressources") {
            if (carte.couleur == "marron") {
                //vert est debloqué
                // jouer la carte
                partie.terrainJ1.brown[1].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "vert" && debloquerLaCouleurSuivante(partie.terrainJ1.brown[0], 1)) {
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "bleu" && debloquerLaCouleurSuivante(partie.terrainJ1.green[0], 1)) {
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "rouge" && debloquerLaCouleurSuivante(partie.terrainJ1.pink[0], 10)) {
                JouerCarte(carte, "jouer")
                return true;

            }

        } else if (carte.type == "dieu" || carte.type == "piege") {
            if (carte.couleur == "marron") {
                //vert est debloqué
                // jouer la carte
                partie.terrainJ1.brown[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "vert" && debloquerLaCouleurSuivante(partie.terrainJ1.brown[0], 1)) {
                partie.terrainJ1.green[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;


            } else if (carte.couleur == "bleu" && debloquerLaCouleurSuivante(partie.terrainJ1.green[0], 1)) {
                partie.terrainJ1.green[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "rouge" && debloquerLaCouleurSuivante(partie.terrainJ1.pink[0], 10)) {
                partie.terrainJ1.pink[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            }
        } else {
            return false
        }
    } else if (joueur == "j2") {
        if (carte.type == "start") {
            console.log("start")
            if (carte.couleur == "marron") {
                //vert est debloqué
                // jouer la carte
                partie.terrainJ2.brown[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "vert" && debloquerLaCouleurSuivante(partie.terrainJ2.brown[0], 1)) {
                partie.terrainJ2.green[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "bleu" && debloquerLaCouleurSuivante(partie.terrainJ2.green[0], 1)) {
                partie.terrainJ2.blue[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "rouge" && debloquerLaCouleurSuivante(partie.terrainJ2.pink[0], 10)) {
                partie.terrainJ2.pink[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            }
        } else if (carte.type == "ressources") {
            if (carte.couleur == "marron") {
                //vert est debloqué
                // jouer la carte
                partie.terrainJ2.brown[1].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "vert" && debloquerLaCouleurSuivante(partie.terrainJ2.brown[0], 1)) {
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "bleu" && debloquerLaCouleurSuivante(partie.terrainJ2.green[0], 1)) {
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "rouge" && debloquerLaCouleurSuivante(partie.terrainJ2.pink[0], 10)) {
                JouerCarte(carte, "jouer")
                return true;

            }

        } else if (carte.type == "dieu" || carte.type == "piege") {
            if (carte.couleur == "marron") {
                //vert est debloqué
                // jouer la carte
                partie.terrainJ2.brown[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "vert" && debloquerLaCouleurSuivante(partie.terrainJ2.brown[0], 1)) {
                partie.terrainJ2.green[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;


            } else if (carte.couleur == "bleu" && debloquerLaCouleurSuivante(partie.terrainJ2.green[0], 1)) {
                partie.terrainJ2.green[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            } else if (carte.couleur == "rouge" && debloquerLaCouleurSuivante(partie.terrainJ2.pink[0], 10)) {
                partie.terrainJ2.pink[0].push(carte)
                JouerCarte(carte, "jouer")
                return true;

            }
        } else {
            return false
        }
    }

}