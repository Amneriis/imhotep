class Deck {
    constructor(tabCarte) {
        this.deck = tabCarte;
        this.nbrCarte = tabCarte.length;
        this.defausse = []

    }
    piocher() {
        return this.deck.shift()
    }

    count(type) {
        let count = 0
        this.deck.forEach(element => {
            if (element.type == type) { //start *5 
                count++
            }
        });

        return count
    }

    init() {
        this.deck.forEach(element => { //pr chaque carte de deck je fais
            if (element.type == "start") { //start *5 
                for (let i = 0; i < 2; i++) {
                    this.deck.push(element)

                }
            } else if (element.type == "ressource") { // 7* ressource
                if (element.couleur == "rouge") { //si les carte sont rouge juste 5 fois
                    for (let i = 0; i < 5; i++) {
                        this.deck.push(element)

                    }
                } else {
                    for (let i = 0; i < 7; i++) {
                        this.deck.push(element)

                    }
                }

            }
            if (element.type == "piege") { //piege *3
                for (let i = 0; i < 3; i++) {
                    this.deck.push(element)

                }
            }
        });


        this.nbrCarte = this.deck.length
    }






    melange() {
        let x
        // je créer le tableau qui sera mélangé
        let tableau2 = [];
        let taille = this.deck.length;
        for (let i = 0; i < taille; i++) {
            do {
                x = Math.floor(Math.random() * taille);
            } while (tableau2[x] != undefined);
            // tableau2[x] <- tableau[i]
            tableau2[x] = this.deck[i];

        }
        this.deck = tableau2
        return this.deck;
    }

    save() {


    }
}