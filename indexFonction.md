# Projet Imhotep | Index fonction et variable

__function preload() =__ Fonction pour précharger les objets
-
__function background() =__ Fonction pour le background du jeu
-
__function regle() =__  Fonction règle du jeu
-
__function verifCouleur() =__ Verifie la couleur de la carte et retourne un bouléan
-
__function veriftype() =__ Vérifie le type de la carte et retourne un bouléan
-
__function debloquerLaCouleurSuivante() =__ Fonction qui defini si la colone suivant de couleur est debloquer
-
__function finDeTour() =__ Fonction qui définie la fin d'un tour
-
__function joueurtour() =__ Fonction qui calcule le nombre de tour
-
__function tour() =__ Fonction qui définie qui doit jouer
-
__function game()=__ Fonction paramètre du jeu
-
__function pageAccueil()=__ commande qui affiche l'image de background
-
__function victoire()=__ Fonction de victoire
-
__function defaite()=__ Fonction de defaite
-
__function mouseclick()=__ Fonction qui gère les positions des mouses clicks
-
__function pageRegles()=__ Fonction qui gère les paramètres de la page de règles
-
__function pageOptions()=__ Fonction qui gère les paramètres de la page d'options
-
__function pagedeJeu()=__ Fonction qui gère les paramètres de la page de jeu
-
__function pageDeResultat()=__ Fonction qui gère les paramètres de la page de résultat
-