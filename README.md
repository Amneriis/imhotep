# Project Imothep

Le **project Imothep** est un essaie de portage du jeu de société **Imothep** du meme nom

# Structure

Le projet se présente de telle sorte:

### coté Client

- Public
  - css - img - js

### coté Serveur

- server - db - model - routes

### Serveur
index.js

### Views
index.html

# Lancer le jeu

npm i

ouvrir son navigateur sur: localhost:3000
