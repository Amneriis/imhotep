
// fichier qui contiendra toutes les routes qui modifie la partie
var express = require('express');
var router = express.Router();

const path = require("path");



//db
router.get("/partie", function (req, res) {
    res.sendFile(path.join(__dirname + "/../db/partie.json"))

})



//APP.POST POUR RECUP LE CHANGEMENT DE CARTE
router.get("/pioche", function (req, res) {
    res.redirect('/')
});


module.exports = router;